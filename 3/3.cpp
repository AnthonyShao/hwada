#include<iostream>

struct Event {int l,r,v;};
int T, n, m, q;             //T<-test, n<-gamer, m<-mines, q<-events
void Achieved(int* G, int* OD, const Event* E,const int* B, int pe, int num, int sn, int* num1, int* num2){ //check gamer achieved before pe
     int* TV=new int[m];    //mine total value of evnet after pe
     int* Sum=new int[n];   //total value of each gamer
     int* temp=new int[n];  //temp to change rich order
     for(int i=0;i<m;i++)
        TV[i]=0;
     for(int i=0;i<n;i++)
         Sum[i]=0;
     for(int i=0;i<pe;i++){           //pe is middle pivot event and put all event value to corresponding TV[]
            TV[E[i].l-1]+=E[i].v;     
            if(E[i].r<m)
               TV[E[i].r]-=E[i].v;
            }  //need if for boundary
     for(int i=1;i<m;i++)             //make TV[i] as the sum before TV[i] to calculate total value of each mine
            TV[i]+=TV[i-1]; 
     for(int i=0;i<m;i++)             //to calculate total value of each gamer
            Sum[B[i]-1]+=TV[i];
     for(int i=0;i<num;i++)           //put all gamer achieved goal to OD[] array from 1 to num1 and not achieved from num1+1 to num1+num2
        if(Sum[OD[sn+i-1]-1]>=G[OD[sn+i-1]-1]){
             temp[(*num1)]=OD[sn+i-1];
             (*num1)++;}                            
        else if(Sum[OD[sn+i-1]-1]<G[OD[sn+i-1]-1]){
             temp[num-(*num2)-1]=OD[sn+i-1];
             (*num2)++;}                      
     for(int i=0;i<num;i++){
        OD[sn+i-1]=temp[i];
        }
     delete []TV;
     delete []Sum;
     delete []temp;
     }

void when(int* AC, int* G, int* OD,const Event* E,const int* B, int num, int sn,int pe,int qe){  
                                          //num<-gamer who will achived goal in event interval pe to qe
                                          //sn<- start number from order array(we only have to check gamer from OD[sn-1] to OD[sn+num-1])
     if(num<=0){                          //if no gamer in this event interval
        return;}
     int num1=0;                          //num1<-gamer achieved goal in pe to re interval
     int num2=0;                          //num2<-gamer not achieved goal before re
     int re=(qe+pe)/2; 
     Achieved(G,OD,E,B,re,num,sn,&num1,&num2);  //calculate whether goal will be achieved before event re
     if((qe-pe)<=0){                            //if event interval=1 then set AC[] of num1 to qe event
        for(int i=0;i<num1;i++){
           *(AC+*(OD+i+sn-1)-1)=qe;
           }
        return;}
     when(AC,G,OD,E,B,num1,sn,pe,re);    //checked goal achieved gamer in num1 will achieved their goal at which event
     when(AC,G,OD,E,B,num2,sn+num1,re+1,qe);    //check not achieved gamer from sn+num1 to sn+num1+num2 will achieved goal after re
     }

int main(){

    scanf("%d",&T);
    while(T--){
       scanf("%d%d%d",&n,&m,&q);
       int* G= new int[n];            //Gaol for each gamer
       int* B= new int[m];            //mines Belong to whom
       int* C= new int[n];            //Cumulative coins of each gamer
       int* AC=new int[n];            //is Achieved array with element = achieved event(-1, means not achieved) 
       int* OD=new int[n];            //gamer order array which is sorted by event
       Event* E=new Event[q];         //event array
       for(int i=0;i<n;i++){
            scanf("%d",&G[i]);
            C[i]=0;                   
            AC[i]=-1;                 //initialized with -1
            OD[i]=i+1;}               //initialized 1~n
       for(int i=0;i<m;i++)
            scanf("%d",&B[i]);    
       for(int i=0;i<q;i++)
            scanf("%d%d%d",&E[i].l,&E[i].r,&E[i].v);
       when(AC,G,OD,E,B,n,1,1,q);     //calculate when will goal achived 

       printf("%d",AC[0]);
       for(int i=1;i<n;i++)
           printf(" %d",AC[i]); 
       printf("\n");   
    delete []G;
    delete []B;
    delete []C;
    delete []AC; 
    delete []E;                     
    }
    return 0;
    }
