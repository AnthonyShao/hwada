#include<iostream>
#include<math.h>


int main(){
    int Test=0;
    int n,m;
    int mp;             //map in integer form
    scanf("%d",&Test);
    while(Test--){
        scanf("%d %d",&n,&m);
        char* map=new char[n*m];
        for(int i=0;i<n;i++)
            scanf("%s",&map[i*m]);
   
        int pre=(1<<(m+1));
        long Tetriz[n*m*pre];
        
        mp=(map[n*m-1]=='.'?0:1);                                 //map in integer ex: .X..=2 (X at i digit will add 2^i) 
        for(int i=n-1;i>=0;i--)
             for(int j=m-1;j>=0;j--){
                  mp=mp<<1;
                  if(map[i*m+j]=='X')
                       mp++;  
                  }
        
        Tetriz[0]=(mp&1)>0?0:1;              
        for(int i=0;i<n;i++){                                      //Tetriz[(i*m+j)*2^(m+1)+k] is the value for matrix
            for(int j=0;j<m;j++)                                   //with all possible before [i,j] with m+1 previous block in k state
               if(j>0)
                 for(int k=0;k<(2<<((i*m+j)>(m+1)?(m+1):(i*m+j)));k++){                  // where k is with same representation as mp
                       int temp=(i*m+j-1)>(m+1)?(m+1):(i*m+j-1);
                       if(Tetriz[(i*m+j-1)*pre+k*2]>0)                                         //+ways to put 1 extra 1x1 block
                            Tetriz[(i*m+j)*pre+k]=Tetriz[(i*m+j-1)*pre+k*2];
                       else 
                            Tetriz[(i*m+j)*pre+k]=(mp&(i*m+j))>0?0:1;
                       if(j>0&&(mp&(i*m+j-1))==0){                                                  //+ways to put 1 extra horizontal 2x1-blocks
                           Tetriz[(i*m+j)*pre+k]+=((k*2)>(1<<(temp))?0:Tetriz[(i*m+j-1)*pre+k*2]); }
                       if(i>0&&(mp&((i-1)*m+j))==0){                                                //+ways to put 1 extra vertical 1x2-blocks
                           Tetriz[(i*m+j)*pre+k]+=((k*2)&2>0?0:Tetriz[(i*m+j-1)*pre+k*2+2]);}
                       if(i>1&&j>1&&(mp&(i*m+j-1))==0&&(mp&((i-1)*m+j))==0&&(mp&((i-1)*m+j-1)==0)){ //+ways to put 1 extra 2x2 cube
                           Tetriz[(i*m+j)*pre+k]+=((k*2)&(3+(1<<temp)>0?0:Tetriz[(i*m+j-1)*pre+k*2+3])); }
                       Tetriz[(i*m+j)*pre+k]=Tetriz[(i*m+j)*pre+k]%1000000007; 
                       printf("%ld\n",Tetriz[(i*m+j)*pre+k]);
                       }
                }
    printf("%d %d %d %ld",n,m,pre,Tetriz[n*m*pre-pre]);
    system("pause");
    }
    
    return 0;
}
