#include<iostream>
const int N=100000;
struct Box{	int w, h;};
long long count=0;
Box nbox[N];
Box* pbox=nbox;

int countPivot(const Box* abox, int size2, int pivot_w){    //count group2 with same pivot w as group1
    int pivotNum=0;
    for(int i=0;i<size2;i++)
            if((abox+i)->w==pivot_w)
                  pivotNum++;
    return pivotNum;
    }
    
void mergePair(Box* abox,int size1,int size2,int pivot_w){      //merge 2 groups and count boxes between 2 groups
     int ptr1=0, ptr2=0;                                        //ptr1->checked group1 boxes, ptr2->checked group2 boxes 
     int pivotNum=countPivot(abox+size1,size2,pivot_w);         //count group2 with same pivot w as group1
     Box* temp=new Box[size1+size2];                            
     while(ptr1+ptr2<size1+size2){                              //count all boxes in group1+2
     if(ptr1>=size1 && ptr2<size2){                             //group1 no boxes but group2 still have
         temp[ptr1+ptr2]=*(abox+size1+ptr2);                    //save box in group2 with largest h
         ptr2++;}                                               //remove 1 box from group2
     else if(ptr2>=size2 && ptr1<size1){                        //group2 no boxes but group1 still have
         temp[ptr1+ptr2]=*(abox+ptr1);                          //save box in group2 with largest h
         ptr1++;}                                               //remove 1 box from group1
     else{                                                      //group1,group2 both have boxes
           if((abox+ptr1)->h <= (abox+size1+ptr2)->h){          //group1 box with largest h <= group2 largest h  
                      temp[ptr1+ptr2]=*(abox+size1+ptr2);       //save that box in group2
                      if(pivot_w==(abox+size1+ptr2)->w)         //if group2 largest h box have same pivot w then remove from pivot count
                        pivotNum--;  
                      count=count+size1-ptr1;                   //count=count + all boxes left in group1
                      ptr2++;}                                  //remove first box in group2
           else{                                                //group1 1st box h > group2 1st box h 
                if((abox+ptr1)->w==pivot_w)                     //only count pivot boxes in group2
                        count+=pivotNum;
                temp[ptr1+ptr2]=*(abox+ptr1);                   //save group1 1st box
                ptr1++;                                         //remove that box   
                }
          }
     }
        
     for(int i=0;i<size1+size2;i++)                             //return boxes with order h(from largest to smallest)
          *(abox+i)=temp[i];
     delete [] temp;
     }
    
void cPair(Box* abox, int size){                           //calculate box pairing
     if(size==1)
        return;
     int size1=size/2, size2=size-size1;
     int pivot_w=(abox+size1-1)->w;                        //pivot of w to calculate where 2 groups might have same w
     cPair(abox,size1);                                    //group1 with smaller w boxes
     cPair(abox+size1,size2);                              //group2 with larger w boxes
     mergePair(abox,size1,size2,pivot_w);                  //merge 2 groups through order of h and count number of
     }     
     

void mergeBw(Box* pbox, int size1, int size2){              //merge sort merge function
     Box* temp =new Box[size1+size2];
     int ptr1=0, ptr2=0;
     while(ptr1+ptr2 < size1+size2){
     if(ptr1<size1 && (pbox+ptr1)->w <= (pbox+size1+ptr2)->w || ptr1<size1 && ptr2>=size2)
             {temp[ptr1+ptr2]=*(pbox+ptr1);
             ptr1++;}
     else if(ptr2<size2 && (pbox+size1+ptr2)->w < (pbox+ptr1)->w || ptr2<size2 && ptr1>=size1)
             {temp[ptr1+ptr2]=*(pbox+size1+ptr2);
             ptr2++;}
     }
     for(int i=0;i<size1+size2;i++)
         *(pbox+i)=temp[i];
     }

void mergeSortBw(Box* pbox, int size){         //merge sort for pre sort box(w,h) by w
     if(size==1)
        return;
     int size1=size/2, size2=size-size1;
     mergeSortBw(pbox, size1);
     mergeSortBw(pbox+size1, size2);
     mergeBw(pbox, size1, size2);
     }


int main()
{
	int test=0;
	int box_n=0;
	clock_t start, finish;
    double duration;
	scanf("%d",&test);
	while(test--){
       scanf("%d",&box_n);
       for(int i=0;i<box_n;i++){                          
           scanf("%d%d",&(nbox[i].w),&(nbox[i].h));
           if(nbox[i].w<nbox[i].h){
               int temp=nbox[i].w;
               nbox[i].w=nbox[i].h;
               nbox[i].h=temp;
               }
           }
    mergeSortBw(pbox,box_n);                                     //pre sort nbox by w
    cPair(pbox,box_n);                                           //count boxes paring
    for(int i=0; i<box_n-1;i++){                                 //double count boxes with same w and h
         int rp=0;
         while(i+rp<box_n && (pbox+i)->w==(pbox+i+rp+1)->w && (pbox+i)->h==(pbox+i+rp+1)->h)
            rp++;
         if(rp>0)
            count=count+(rp*(rp+1))/2;
         i+=rp;}
    printf("%lld\n",count);

    count=0;
    }
    return 0;
}
