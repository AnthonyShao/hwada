#include<iostream>

void reverse(char* words,int length){            //reverse string
     char temp[length];
     for(int i=0;i<length;i++)
         temp[length-i-1]=*(words+i);
     for(int i=0;i<length;i++)
         *(words+i)=temp[i];
     }

int main(){
    int Test=10;                      
    int m=2000,n=2000;
    int a=0,b=0;
    scanf("%d",&Test);                           //input Test number

    while(Test--){
         char aWords[m];
         char bWords[n];
         for(int i=0;i<m;i++)
                aWords[i]='\0';
         for(int j=0;j<n;j++)
                bWords[j]='\0';
         scanf("%s",aWords);                      //input string a
         scanf("%s",bWords);                      //input string b
         for(int i=0;i<m;i++)
            if(aWords[i]=='\0'){
               a=i;
               break;}
         for(int j=0;j<n;j++)
            if(bWords[j]=='\0'){
               b=j;
               break;}       
         reverse(aWords,a);                     //reverse input to get LCS from head to make lexicographically samllest
         reverse(bWords,b);
         int* LCS=new int[(a+1)*(b+1)];             //to save LCS count in LCS[i,j] with i*(b+1)+j index
         for(int i=0;i<=a;i++){                             
             LCS[i*(b+1)]=0; }
         for(int j=1;j<=b;j++){
             LCS[j]=0; }
             
         for(int i=1;i<=a;i++)                                         //fill up LCS table
            for(int j=1;j<=b;j++){
                if(aWords[i-1]==bWords[j-1]){LCS[i*(b+1)+j]=LCS[(i-1)*(b+1)+(j-1)]+1;}     //when LCS with xi=yj  -> LCS[i,j]=LCS[i-1,j-1]
                else{
                     if(LCS[(i-1)*(b+1)+j]>LCS[i*(b+1)+(j-1)]){            //when LCS with xi=/=yj   LCS[i,j]=max{LCS[i-1,j],LCS[i,j-1]}
                          LCS[i*(b+1)+j]=LCS[(i-1)*(b+1)+j];}
                     else{
                          LCS[i*(b+1)+j]=LCS[i*(b+1)+(j-1)];}                         
                     }
                 }  //end for(j)

          int len_lcs=LCS[a*b+a+b];                             
          char* lcsWords= new char[len_lcs];
          int lcs_i=a;
          int lcs_j=b;        
          for(int l=0;l<len_lcs;l++)                                   //save LCS with largest lexicogrphically 
               for(int k=0;k<26;k++){                                  //check from a to z
                   int temp_i=lcs_i;
                   int temp_j=lcs_j;
                   while(temp_i>0&&aWords[temp_i-1]!='a'+k)
                         temp_i--;
                   while(temp_j>0&&bWords[temp_j-1]!='a'+k)
                         temp_j--;
                   if(LCS[temp_i*(b+1)+temp_j]==len_lcs-l){
                          lcsWords[l]='a'+k;
                          lcs_i=temp_i-1;
                          lcs_j=temp_j-1;
                          break;}
                   }
              for(int i=0;i<len_lcs;i++)
                       printf("%c",lcsWords[i]);
              printf("\n");
              delete[]lcsWords;
              delete[]LCS;
          }  //end Test
    return 0;
    }
