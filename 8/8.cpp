#include <cstdio>
#include <queue>
const int N=100;
const int white=0, gray=1, black=2, ex=0, ey=1, ez=2;
struct room{  int color, dist, sum_bev, x, y, z; char state; room* pi;
void setLocation(int xin, int yin, int zin){x=xin; y=yin; z=zin;} };
int link[6][3]={{-1,0,0},{1,0,0},{0,-1,0},{0,1,0},{0,0,-1},{0,0,1}};  //6 direction to walk from current room
using namespace std;
int Test,n;
room map[N][N][N];
int main(){
    scanf("%d",&Test);
    /*FILE* pfile;////////
    pfile=fopen("test.txt","r");////////
    if(pfile==NULL)/////////
        return 0;////////// */
    while(Test--){
        queue<room*> checkroom;
        scanf("%d",&n);
        room start;
        char line[n];
        for(int i=0;i<n;i++)
            for(int j=0;j<n;j++){  //fscanf(pfile,"%s",line);////////////
                scanf("%s", &line);
                for(int k=0;k<n;k++){          //set all room attributes and start room
                    map[i][j][k].state=line[k];
                    map[i][j][k].color=white;
                    map[i][j][k].dist=-1;
                    map[i][j][k].pi=NULL;
                    map[i][j][k].sum_bev=0;
                    map[i][j][k].setLocation(i,j,k);
                    if(map[i][j][k].state=='S')
                        start.setLocation(i,j,k);  //printf("%d,%d,%d:%d,%d,%d,%c;",i,j,k,map[i][j][k].x,map[i][j][k].y,map[i][j][k].z,map[i][j][k].state);
                      }
               }
        map[start.x][start.y][start.z].color=gray;
        map[start.x][start.y][start.z].dist=0;
        map[start.x][start.y][start.z].sum_bev=0;
        checkroom.push(&map[start.x][start.y][start.z]);
        room* current;
        room* next;
        room end;
        end.dist=-1;     //printf("start%d,%d,%d,%c\n",start.x,start.y,start.z,start.state);
        while(!checkroom.empty()){
            current=checkroom.front();   //printf("check%d,%d,%d,%c\n",current->x,current->y,current->z,current->state);
            checkroom.pop();
            if(current->state=='E')
                 end=*current;
            for(int i=0;i<6;i++){
                if( (current->x+link[i][ex])>=0 && (current->x+link[i][ex])<n && (current->y+link[i][ey])>=0 && (current->y+link[i][ey])<n 
                && (current->z+link[i][ez])>=0 && (current->z+link[i][ez])<n){
                     next=&map[current->x+link[i][ex]][current->y+link[i][ey]][current->z+link[i][ez]];  //printf("%d,%d,%d,%c,%d\n",next->x,next->y,next->z,next->state,next->sum_bev);
                     int temp_bev=next->state=='B'?1:0;
                     if( next->state!='#' && next->color==white ){  //found a new room
                              next->color=gray;
                              next->dist=current->dist+1;
                              next->pi=map[current->x][current->y]+current->z;
                              next->sum_bev=current->sum_bev+temp_bev;
                              checkroom.push(next); //printf("%d,%d,%d new push in que\n",checkroom.front().x,checkroom.front().y,checkroom.front().z);
                              }
                      else if( next->state!='#' && next->color==gray){      //an old room but with a larger sum_bev & same distance
                                 if( next->dist == (current->dist+1) && next->sum_bev < (current->sum_bev + temp_bev)){
                                     next->pi=current;
                                     next->sum_bev=current->sum_bev+temp_bev;}
                              }
                      //printf("%d,%d,%d,%c,%d\n",next->x,next->y,next->z,next->state,next->sum_bev);
                    }//end if
                }//end for
            current->color=black;   //printf("checkdone%d,%d,%d\n",current->x,current->y,current->z);
        }
    if(end.dist==-1)
        printf("Fail OAQ\n");
    else
        printf("%d %d\n",end.dist,end.sum_bev);
    }  //fclose(pfile);//////////////
    //getchar();
    return 0;
    }
