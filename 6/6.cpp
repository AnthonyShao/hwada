#include<iostream>

void merge(int* x, int size1, int size2){
     int* temp=new int[size1+size2];
     int xr1=0, xr2=0;
     while(xr1+xr2 < size1+size2){
     if(xr1<size1 && *(x+xr1) <= *(x+size1+xr2) || xr1<size1 && xr2>=size2)
             {temp[xr1+xr2]=*(x+xr1);
             xr1++;}
     if(xr2<size2 && *(x+size1+xr2) < *(x+xr1) || xr2<size2 && xr1>=size1)
             {temp[xr1+xr2]=*(x+size1+xr2);
             xr2++;}
     }
     for(int i=0;i<size1+size2;i++)
         *(x+i)=temp[i];
     }

void mergeSort(int* x,int size){
     if(size==1)
        return;
     int size1=size/2, size2=size-size1;
     mergeSort(x, size1);
     mergeSort(x+size1, size2);
     merge(x, size1, size2);
     }

int notLarger(int* x,int size,int k,int d){
    int anchor=*x;
    int k_last=-1;
    for(int i=1;i<size;i++){
         if(i==1 && (*(x+1)-*x)>d){
               anchor=*(x+1);
               k_last=*x;
               k--;}
         else if(k_last<anchor && (*(x+i)-anchor)>d){
               k_last=*(x+i-1);
               k--;
               if((*(x+i)-*(x+i-1))>d)
                     anchor=*(x+i);
               }
         else if(k_last>=anchor && (*(x+i)-k_last)>d){
               anchor=*(x+i);}
         if(i==size-1 && (*(x+i)-k_last)>d){
               k_last=*(x+i);
               k--;}
         //printf("k=%d k_last=%d anchor=%d d=%d,x+i %d\n",k,k_last,anchor,d,*(x+i));
         if(k<0)
               return -1;
         }
    return 1;
    }


int main(){
    int Test,temp_n,k;
    scanf("%d",&Test);
    while(Test--){
       scanf("%d %d",&temp_n,&k);
       int* temp_x=new int[temp_n];
       for(int i=0;i<temp_n;i++)
           scanf("%d",&temp_x[i]);
       mergeSort(temp_x,temp_n);                       //sort xi by location
       
       int* x=new int[temp_n];
       int n=temp_n;
       int i=1;
       x[0]=temp_x[0];
       for(int temp_i=1;temp_i<temp_n;temp_i++){       //delete repeted cities with same location
           if(temp_x[temp_i]==temp_x[temp_i-1])
                n--;
           else{
                x[i]=temp_x[temp_i];
                i++;}
            }
       
       int p=0,q=x[n-1];
       while(p!=q){
           if((notLarger(x, n, k,(q+p)/2))>0){     //max distance not larger than (q-p)/2 then q to (q-p)/2
                 q=(q+p)/2;} 
           else{                                   //max distance larger than (q-p)/2 then p to (q-p)/2+1
                 p=(q+p)/2+1;}                     
           }      
       if(n<=k)
           p=0;
       printf("%d\n",p);
       
       }
    return 0;
    }
