#include<iostream>

struct coin{int avb,use,value;};

int main(){
    int T;
    coin c[10];
    scanf("%d",&T);    
    while(T--){
       c[0].value=1,c[1].value=5,c[2].value=10,c[3].value=20,c[4].value=100,c[5].value=200,c[6].value=1000,c[7].value=2000,c[8].value=50,c[9].value=500; 
       int inputC[10];
       int inputP;
       scanf("%d %d %d %d %d %d %d %d %d %d %d",&inputP,&inputC[0],&inputC[1],&inputC[2],&inputC[3],&inputC[8],&inputC[4],&inputC[5],&inputC[9],&inputC[6],&inputC[7]);
       int count[4]={0,0,0,0};    //count[0] mod 500:50 ->0:0  count[1] mod 500:50 ->1:0 count[2] mod 500:50 ->0:1 count[3] mod 500:50 ->1:1
       int q[4]={0,0,0,0};
       int p[4]={0,0,0,0};
              
       for(int k=0;k<4;k++){
          for(int i=0;i<10;i++){              //set up inputC to c[i].avb/use
               c[i].use=inputC[i];
               c[i].avb=0;
               q[k]+=c[i].use*c[i].value;}
          p[k]=inputP;

          if(k==2||k==3){                     // consider 1 50 coin in use
               if(inputC[8]>0&&p[k]>c[8].value){
                     p[k]-=c[8].value;
                     q[k]-=c[8].value;
                     c[8].use-=1;
                     count[k]++;}
               else{
                     q[k]=0;}
               }
          if(k==1||k==3){                     //consider 1 500 coin in use
               if(inputC[9]>0&&p[k]>c[9].value){
                     p[k]-=c[9].value;
                     q[k]-=c[9].value;
                     c[9].use-=1;
                     count[k]++;}
               else{
                     q[k]=0;}
               }
          int temp50,temp500;     
          //printf("%d %d %d\n",q[k],p[k],k);
          if(q[k]>=p[k]){
             
             for(int i=7;i>=0;i--){                  

                 if(i==4){                           //change 50 coin to 100
                      temp50=(c[8].use/2);
                      c[8].avb+=c[8].use-temp50*2;
                      c[8].use-=c[8].use-temp50*2;
                      q[k]-=(c[8].use-temp50*2)*c[8].value;
                      c[4].use+=temp50;
                      c[8].use-=temp50*2;}
                 else if(i==6){
                      temp500=(c[9].use/2);
                      c[9].avb+=c[9].use-temp500*2;
                      c[9].use-=c[9].use-temp500*2;
                      q[k]-=(c[9].use-temp500*2)*c[9].value;
                      c[6].use+=temp500;
                      c[9].use-=temp500*2;}         //change 500 coin to 1000
                 
                 if((q[k]-p[k])>=c[i].use*c[i].value){     //all c[i].avb will not be used
                      q[k]-=c[i].use*c[i].value;
                      c[i].avb+=c[i].use;
                      c[i].use=0;}   
                 else{                               //or move c[i].avb-change to c[i].use
                      int change=(q[k]-p[k])>=0?(q[k]-p[k])/c[i].value:0;
                      c[i].use-=change;
                      c[i].avb+=change;
                      q[k]-=change*c[i].value;}
                 //printf("%d_%d\n",c[i].value,c[i].use);
                 
                 if(i==4){
                      if(c[4].use>=temp50){
                            q[k]-=c[8].avb*c[8].value;
                            c[4].use-=temp50;
                            c[8].use+=temp50*2;}
                      else{
                            q[k]-=c[8].avb*c[8].value;
                            c[8].use+=temp50*2;
                            c[4].use=0;}    
                      }
                      
                 else if(i==6){
                      if(c[6].use>=temp500){
                            q[k]-=c[9].avb*c[9].value;
                            c[6].use-=temp500;
                            c[9].use+=temp500*2;}
                      else{  
                            q[k]-=c[9].avb*c[9].value;
                            c[9].use+=temp500*2;
                            c[6].use=0;}
                      }
                 //printf("in i %d %d %d %d %d %d %d %d %d %d\n",i,k,p[k],q[k],c[0].avb,c[0].use,c[0].value,c[1].avb,c[1].use,c[1].value);     
                 }                                  //end for (i)
             }                                      //end if (no need to do q<p)
             for(int i=0;i<10;i++){
                   count[k]+=c[i].use;
                   //printf("%d_%d\n",c[i].value,c[i].use);
                   }
             if(p[k]!=q[k])
                   count[k]=-1;
          }                                         //end for (k)
       int result=-1;
       for(int k=0;k<4;k++){
              //printf("%d\n",count[k]);
              if(count[k]>result)
                    result=count[k];
                    }
                    
       printf("%d\n",result);    
       }           //end while
    return 0;
    }
