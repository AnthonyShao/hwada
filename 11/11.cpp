#include <cstdio>
#include <vector>

using namespace std;
struct edge{int a,b;};
bool acceptset(vector<vector<int> >* totalset, vector<vector<int> >* edgelist, int n); //to save all possible set+1 set
bool addtoset(vector<int>* set, vector<vector<int> >* edgelist, int newelement); //to check if add j element to set if ok
void initialset(vector<vector<int> >* totalset, int n);

int main(){
    int T,n,m,maxset;
    scanf("%d",&T);
    while(T--){
         scanf("%d %d",&n,&m);
         edge* e=new edge[m];           
         vector<vector<int> > totalset;
         vector<vector<int> > edgelist;
         
         for(int i=0;i<m;i++)
               scanf("%d %d",&e[i].a,&e[i].b);
        
         for(int i=0;i<n;i++){
                vector<int> selfedge;
                selfedge.push_back(i);
                edgelist.push_back(selfedge); }
     //////printf("%d %d\n",selfedge.front(),edgelist.at(i).front());
         for(int i=0;i<m;i++){
                edgelist.at(e[i].a).push_back(e[i].b);
                edgelist.at(e[i].b).push_back(e[i].a);}
         /*
         for(int i=0;i<n;i++){
               for(int j=0;j<edgelist.at(i).size();j++)
                    printf("%d ", edgelist.at(i).at(j));
               printf("\n");
               }
         getchar();
         */
         for(int k=0;k<n;k++){
               if(k==0)
                   initialset(&totalset, n);
               else if( acceptset(&totalset, &edgelist, n)== false ){ //acceptset save all k+1 element posisble set in totalset
                     maxset=k;
                     break;}
               }
         printf("%d\n",maxset);      
    }
    
    return 0;
    }
    
bool acceptset(vector<vector<int> >* totalset, vector<vector<int> >* edgelist, int n){
     int lastsetnum=totalset->size();
     for(int i=0;i<lastsetnum;i++){              //i set
           for(int j=0;j<n;j++)
               if(addtoset(&totalset->at(i),edgelist,j)==true){
                     vector<int> tempset=totalset->at(i);
                     tempset.push_back(j);
                     totalset->push_back(tempset); 
                     //for(int k=0;k<totalset->back().size();k++)
                     //     printf("%d ",totalset->back().at(k));
                     //printf("\n");     
                     }
           }
     
     //printf("total set size %d lastset num %d\n",totalset->size(),lastsetnum);
     totalset->erase(totalset->begin(),totalset->begin()+lastsetnum);  //remove last round sets
     
     if(totalset->size()==0)
          return false;
     else
          return true;
     }

bool addtoset(vector<int>* set, vector<vector<int> >* edgelist, int newelement){
    for(int i=0;i<set->size();i++){   //check each element in old list
           // printf("set%d setsize%d ",i,set->size());
            for(int j=0;j<(edgelist->at(newelement)).size();j++){            //check adj list of newelement
                    //printf("checknum%d ",j);
                    if((edgelist->at(newelement)).at(j)==set->at(i))
                           return false;
                    }
            //getchar();        
            }
    return true;
    }

void initialset(vector<vector<int> >* totalset, int n){
     for(int i=0;i<n;i++){
             vector<int> tempset;
             tempset.push_back(i);
             totalset->push_back(tempset);
             }
     }
